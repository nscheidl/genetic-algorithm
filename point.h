#ifndef POINT_H
#define POINT_H

#include <vector>
#include <iostream>

template<class T>
class Coordinates2D{
private:
    T x;
    T y;
public:
    Coordinates2D(T a, T b): x(a), y(b) {}
    virtual ~Coordinates2D() = default;
    T getX() const { return x;}
    T getY() const { return y;}
    void setX(T value) { x=value;}
    void setY(T value) { y=value;}
};

class Move{
protected:
    int x;
    int y;
public:
    Move(int a, int b): x(a), y(b) {}
    virtual ~Move() = default;
    int getX() const { return x;}
    int getY() const { return y;}
};

class Point {
private:
    int x;
    int y;
    int score;
    std::size_t current_move;
    std::vector<Move> movement;
public:
    Point(int a, int b): x(a), y(b), score(0), current_move(0) {};
    void addMove(const Move& m) { movement.push_back(m);}
    int getScore() const { return score;}
    int getX() const { return x;}
    int getY() const { return y;}
    void setX(int a) { x=a;}
    void setY(int b) { y=b;}
    void setScore(int s) { score=s;}
    void nextMove() {
        *this = *this + movement[current_move];
        current_move += 1;
    }
    const Move& getMove(int i) const { return movement[i];}
    Point& operator+(const Move& m) { x += m.getX(); y += m.getY(); return *this;}
};

std::ostream& operator<<(std::ostream& f, const Point& p);

#endif
