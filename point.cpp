#include "point.h"

std::ostream& operator<<(std::ostream& f, const Point& p){
    f << "x: " << p.getX() << " ; y: " << p.getY() << std::endl;
    return f;
}
