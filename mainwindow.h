#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include <QTimer>

#include <vector>
#include "point.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void next_step(size_t nb);
    void new_generation();

private:
    Ui::MainWindow *ui;

    QGraphicsScene *scene;
    QBrush goal;
    QTimer *timer;

    std::vector<Point> tab;
    size_t current_move;
    int generation;
};
#endif // MAINWINDOW_H
