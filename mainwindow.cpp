#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>
#include <ctime>
#include <unistd.h>
#include <stdlib.h>
#include <cmath>
#include <set>

#include <QPropertyAnimation>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , scene(new QGraphicsScene(this))
    , goal(QBrush("green"))
    , current_move(0)
    , timer(new QTimer(this))
    , generation(1)
{
    srand(time(NULL));
    ui->setupUi(this);
    ui->graphicsView->setScene(scene);
    QRectF sceneRect = QRectF(0,0,ui->graphicsView->width()-20,ui->graphicsView->height()-20)  ;
    ui->graphicsView->setSceneRect(sceneRect);
    ui->graphicsView->setBackgroundBrush(QBrush("white"));
    ui->graphicsView->setResizeAnchor(QGraphicsView::AnchorViewCenter);
    scene->addRect(QRect(ui->graphicsView->width()/2 - 5, 0,10,10), QPen(), goal);
    ui->lcdGeneration->display(generation);

    for(size_t i=0; i<200; ++i){
        tab.push_back(Point(ui->graphicsView->width()/2,ui->graphicsView->height()*3/4));
        scene->addRect(tab[i].getX(),tab[i].getY(),1,1);
        for(size_t j=0; j<1000; ++j){
            int x=rand() % 3 - 1;
            int y=rand() % 3 - 1;
            tab[i].addMove(Move(x, y));
        }
    }
    connect(timer, &QTimer::timeout, this, [=]() { next_step(10);});
    timer->start(50);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::next_step(size_t nb)
{
    size_t i = 0;
    while (i < nb && current_move < 1000){
    //for(size_t i = 0; i < nb; i++){
        //sleep(1);
        current_move ++;
        scene->clear();
        for(std::vector<Point>::iterator it = tab.begin(); it!=tab.end(); ++it){
            (*it).nextMove();

        }
        scene->addRect(QRect(ui->graphicsView->width()/2 - 5, 0,10,10), QPen(), goal);
        for(std::vector<Point>::iterator it = tab.begin(); it!=tab.end(); ++it){
            scene->addRect(QRect((*it).getX(),(*it).getY(),1,1));
        }
        update();

        i++;
    }

    if(current_move == 1000){
        timer->stop();
        new_generation();
        timer->start(50);
    }
    //std::cout<< current_move << std::endl;
}

void MainWindow::new_generation(){
    generation++;
    //Calcul du score de chaque point
    std::list<int> distances;
    for(std::vector<Point>::iterator it = tab.begin(); it!=tab.end(); ++it){
        int distance = sqrt(
                    ((*it).getX() - ui->graphicsView->width()/2) * ((*it).getX() - ui->graphicsView->width()/2)+
                    ((*it).getY() * (*it).getY()) );
        (*it).setScore(distance);
        distances.push_back(distance);
    }
    distances.sort();
    distances.resize(10);
    //std::cout << "Distance max acceptée " << distances.back() << std::endl;
    //std::cout << "Meilleure distance " << distances.front() << std::endl;
    std::vector<Point> newPoints;
    for(std::vector<Point>::iterator it = tab.begin(); it!=tab.end(); ++it){
        if((*it).getScore() <= distances.back()){
            (*it).setX(ui->graphicsView->width()/2);
            (*it).setY(ui->graphicsView->height()*3/4);
            newPoints.push_back(*it);
        }
    }

    int size = newPoints.size();
    tab.clear();
    tab = std::vector<Point>(newPoints);
    current_move = 0;
    scene->clear();
    scene->addRect(QRect(ui->graphicsView->width()/2 - 5, 0,10,10), QPen(), goal);
    for(size_t i = size; i<200; i++){
        tab.push_back(Point(ui->graphicsView->width()/2,ui->graphicsView->height()*3/4));
        scene->addRect(tab[i].getX(),tab[i].getY(),1,1);
        for(size_t j=0; j<1000; ++j){
            int x=rand() % size;
            tab[i].addMove(tab[x].getMove(j));
        }
    }
    //std::cout << "Génération finie" << std::endl;
    ui->lcdGeneration->display(generation);
    update();
}


